package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// 支店定義ファイル用正規表現
	private static final String REGULAR_BRANCH_NUM = "[0-9]{3}";

	// 商品定義ファイル用正規表現
	private static final String REGULAR_PRODUCT_NUM = "[A-Za-z0-9]{8}";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String BRANCH_ERROR = "支店";
	private static final String COMMODITY_ERROR = "商品";
	private static final String FILE_DEFINITION_EXIST = "定義ファイルが存在しません";
	private static final String FILE_DEFINITION_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_RCD_FORMAT = "のフォーマットが不正です";
	private static final String FILE_BRANCH_NUM = "の支店コードが不正です";
	private static final String FILE_COMMODITY_NUM = "の商品コードが不正です";
	private static final String FILENAME_SERIAL_NUM = "売上ファイル名が連番になっていません";
	private static final String AMOUNT_OVER_TEN = "合計金額が10桁を超えました";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//コマンドライン引数が設定されているか確認
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();

		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();

		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, REGULAR_BRANCH_NUM, BRANCH_ERROR)) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, REGULAR_PRODUCT_NUM, COMMODITY_ERROR)) {
			return;
		}

		// 集計処理開始
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		BufferedReader br = null;

		// 指定したパスにある売上ファイルを抽出
		for(int i = 0; i < files.length ; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		// 売上ファイルの連番確認
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			if((latter - former) != 1) {
				System.out.println(FILENAME_SERIAL_NUM);
				return;
			}
		}

		// 売上ファイルの内容を抽出
		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				ArrayList<String> rcdContents = new ArrayList<String>(); //rcdファイルの中身
				String line;
				// 一行ずつ読み込む
				while((line = br.readLine()) != null) {
					rcdContents.add(line);
				}
				// 売上ファイルのフォーマットを確認
				String rcdFileName = rcdFiles.get(i).getName();
				if(rcdContents.size() != 3) {
					System.out.println(rcdFileName + FILE_RCD_FORMAT);
					return;
				}

				// 売上ファイルの支店コードが支店定義ファイルに該当しているか確認
				String branchCode = rcdContents.get(0);
				if(!branchNames.containsKey(branchCode)) {
					System.out.println(rcdFileName + FILE_BRANCH_NUM);
					return;
				}

				// 売上ファイルの商品コードが商品定義ファイルに該当しているか確認
				String commodityCode = rcdContents.get(1);
				if(!commodityNames.containsKey(commodityCode)) {
					System.out.println(rcdFileName + FILE_COMMODITY_NUM);
					return;
				}

				// 売上ファイルの売上金額が数字であるか確認
				if(!rcdContents.get(2).matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				// 合計金額が10桁以内か確認
				long sale = Long.parseLong(rcdContents.get(2));
				Long branchSaleAmount = branchSales.get(branchCode) + sale;
				if(branchSaleAmount >= 10000000000L){
					System.out.println(AMOUNT_OVER_TEN);
					return;
				}
				Long commoditySaleAmount = commoditySales.get(commodityCode) + sale;
				if(commoditySaleAmount >= 10000000000L){
					System.out.println(AMOUNT_OVER_TEN);
					return;
				}

				// 合計金額を支店別集計ファイルと商品別集計ファイルへ出力
				branchSales.put(branchCode, branchSaleAmount);
				commoditySales.put(commodityCode, commoditySaleAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 定義ファイルの読み込み処理
	 *
	 * @param フォルダパス
	 * @param 定義ファイル名
	 * @param 支店コードと支店名を保持するMap、商品コードと商品名を保持するMap
	 * @param 支店コードと売上金額を保持するMap、商品コードと売上金額を保持するMap
	 * @param 定義ファイルのフォーマットを確認する正規表現
	 * @param エラーメッセージ
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> namesMap, Map<String, Long> salesMap, String regularExpression, String error) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			// 定義ファイルが存在するか確認
			if(!file.exists()) {
				System.out.println(error + FILE_DEFINITION_EXIST);
				return false;
			}
			// 定義ファイルの読み込み
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {

				// 文字列を,で分割
				String[] items = line.split(",");

				// 定義ファイルのフォーマット確認
				if((items.length != 2) || (!items[0].matches(regularExpression))){
					System.out.println(error + FILE_DEFINITION_FORMAT);
					return false;
				}
				namesMap.put(items[0], items[1]);
				salesMap.put(items[0], (long) 0);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 集計ファイルの書き込み処理
	 *
	 * @param フォルダパス
	 * @param 集計ファイル名
	 * @param 支店コードと支店名を保持するMap、商品コードと商品名を保持するMap
	 * @param 支店コードと売上金額を保持するMap、商品コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> namesMap, Map<String, Long> salesMap) {
		File file = new File(path, fileName);
		BufferedWriter bw = null;

		try {
			// 集計ファイルへ書き込み
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for(String key : namesMap.keySet()) {
				bw.write(key + "," + namesMap.get(key) + "," + salesMap.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}